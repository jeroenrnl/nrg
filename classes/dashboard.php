<?php

class dashboard {
    private $repo;

    private $id;
    private $name;
    private $title;
    private $datasets=array();
    private $updateJS=array();

    public function __construct(repositoryInterface $repository) {
        $this->repo=$repository;
    }

    public function getAll() {
        return $this->repo->getAll();
    }

    public function getName() {
        return $this->name;
    }

    public function getTitle() {
        return $this->title;
    }

    public function getByName($name) {
        return $this->repo->getByName($name);
    }

    public function getById($id) {
        return $this->repo->getById($id);
    }

    public function display() {
        $dashboards=self::getAll();
        $rows=$this->repo->getWidgets($this->id);
        $tpl=new template("dashboard", array(
            "nav"   => (array) $dashboards,
            "blocks" => (array) $rows
        ));
        $tpl->title=$this->title;

        $tpl->js[]="js/dashboard.js";
        foreach ($rows as $row) {
            foreach ($row as $widget) {
                $init=$widget->getInit();
                if (!empty($init)) {
                    $tpl->script.="\n\n/* " . $widget->getName() . " */\n" . $init . "\n";
                }
                $update=$widget->getUpdate();
                foreach ($update as $dataset => $js) {
                    $this->datasets[$dataset] = $dataset;
                    if (!isset($this->updateJS[$dataset])) {
                        $this->updateJS[$dataset]=array();
                    }
                    $this->updateJS[$dataset][] = $js;
                }
                $tpl->js=array_merge($tpl->js, $widget->getJS());
                $tpl->js=array_unique($tpl->js);
                $tpl->css[]=$widget->getCSS();
            }
        }

        foreach ($this->updateJS as $dataset => $scripts) {
            $js="setInterval(function () {
                $.getJSON('getJSON.php?data=" . $dataset . "', function (data) {\n";

            foreach ($scripts as $script) {
                $js .= $script . ";\n";
            }

            $js .= "}
                );
            }, 5000);\n";

            $tpl->script.="\n" . $js;

        }

        return $tpl;

    }
}
