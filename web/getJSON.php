<?php

error_reporting(E_ALL); ini_set('display_errors', 1);
set_include_path("..");
require_once("include/include.php");


$data=$_GET["data"];

if ($data=="current") {
    $measurement=new collector\dsmr\measurement(new collector\dsmr\repository\repository());

    $measurement=$measurement->getLast();

    echo json_encode($measurement->getData());
} else if ($data=="lasthour") {
    $measurement=new collector\dsmr\measurement(new collector\dsmr\repository\repository());

    $measurement=$measurement->getLastHour();

    echo json_encode($measurement);

} else if ($data=="currentGrowatt") {
    $measurement=new collector\growatt\measurement(new collector\growatt\repository\repository());

    $measurement=$measurement->getLast();

    echo json_encode($measurement->getData());
} else if ($data=="lastGrowatt") {
    $measurement=new collector\growatt\measurement(new collector\growatt\repository\repository());
    echo json_encode($measurement->getLastDay());

} else if ($data=="monthGrowatt") {
    $measurement=new collector\growatt\measurement(new collector\growatt\repository\repository());
    echo json_encode($measurement->getMonthEacToday());
} else if ($data=="monthGas") {
    $measurement=new collector\dsmr\measurement(new collector\dsmr\repository\repository());
    echo json_encode($measurement->getMonthGas());
} else if ($data=="yearGas") {
    $measurement=new collector\dsmr\measurement(new collector\dsmr\repository\repository());
    echo json_encode($measurement->getYearGas());
} else if ($data=="lastEVcharger") {
    echo json_encode(collector\evcharger\charge::getLast(30));


} else {
    echo "No data reqested";
}

?>
