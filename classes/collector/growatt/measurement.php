<?php
namespace collector\growatt;

use repositoryInterface;
use DateTime;

class measurement {
    private $repo;
    public $data=array();

    public function __construct(repositoryInterface $repo) {
        $this->repo=$repo;
    }

    public function readFromTelegram(telegram $telegram) {
        $data=$telegram->getData();

        foreach ($data as $name => $value) {
            if ($value instanceof DateTime) {
                $value=$value->format("Y-m-d H:i:s");
            }

            $this->data[$name]=$value;
        }

    }

    public function getData() {
        return $this->data;
    }

    public function save() {
        $this->repo->save($this);
    }

    public function getLast() {
        return $this->repo->getLast();
    }

    public function getLastDay() {
        return $this->repo->getLastDay();
    }

    public function getMonthEacToday() {
        return $this->repo->getMonthEacToday();
    }


}
