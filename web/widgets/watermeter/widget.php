<?php
class watermeter extends widget implements widgetInterface {
    const NAME="watermeter";

    protected $files=array(
        "css" => "widget.css",
        "js" => array(
                "/widget.js",
                "https://code.jquery.com/jquery-1.9.1.js"
            ),
        "html" => "widget.html"
    );

    public function getUpdate() {
        return array("current" => "watermeter.update(data)");
    }

}
