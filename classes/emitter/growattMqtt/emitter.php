<?php
namespace emitter\growattMqtt;

require_once("vendor/autoload.php");

use collector\growatt\repository\repository;
use collector\growatt\repository\inverterRepository;
use collector\growatt\measurement;

use DateTime;
use DateTimeZone;
use Exception;

use \PhpMqtt\Client\MqttClient;
use \PhpMqtt\Client\ConnectionSettings;

use stdClass;

class emitter {

    public function __construct(
        private MqttClient $mqtt, 
        private ConnectionSettings $settings,
        private string $topic
    ) {}

    /**
     */
    private function sendMQTT() {
        $this->sendAutoDiscover();
        $counter = 0;
        while (true) {
            foreach($this->getMQTT() as $topic => $payload) {
                $this->mqtt->publish($this->topic . "/" . $topic, $payload);
            }
            sleep(60);
            $counter++;

            if ($counter >= 60) {
                $this->sendAutoDiscover();
                $counter = 0;
            }
        }
    }

    /**
     * Runs the data collector service
     */
    public function run() : void {
        openlog("nrg", LOG_PID | LOG_PERROR, LOG_LOCAL0);
        syslog(LOG_INFO, "Growatt MQTT emitter starting");
        $cleanSession = false;
        $this->mqtt->connect($this->settings, $cleanSession);
        syslog(LOG_INFO, "MQTT Connected");

        $this->sendMQTT();

        syslog(LOG_INFO, "Growatt MQTT emitter stopping");
        closelog();
    }

    private function getData() : array {
        $repo = new repository();
        return $repo->getLast()->data;
    }

    private function getInverterData(string $serial) : stdClass {
        $repo = new inverterRepository();
        $repo->load($serial);
        return $repo->data;
    }

    private function getMQTT() : array {
        $d = $this->getData();
        return array(
            "timestamp"         => date_format(date_create($d["datetime"], new DateTimeZone(date_default_timezone_get())), "U"),
            "total_energy"      => (float) $d["EacTotal"],
            "today_energy"      => (float) $d["EacToday"],
            "pv_power"          => (float) $d["Ppv1"],
            "pv_voltage"        => (float) $d["Vpv1"],
            "pv_current"        => (float) $d["Ipv1"],
            "ac_power"          => (float) $d["Pac1"],
            "ac_voltage"        => (float) $d["Vac1"],
            "ac_current"        => (float) $d["Iac1"],
            "ac_frequency"      => (float) $d["Fac"],
            "temperature"       => (float) $d["Temp"]
        );
    }

    private function sendAutoDiscover() : void {
        $topic = "homeassistant/sensor/nrg-growatt/";
        foreach ($this->getAutoDiscover() as $item) {
            $this->mqtt->publish($topic . $item["unique_id"] . "/config", json_encode($item));
        }
    }

    private function getAutoDiscover() : array {
        return array(
            array(
                "unique_id"             => "timestamp",
                "device_class"          => "timestamp",
                "name"                  => "Growatt last update",
                "state_topic"           => "energy/solar/growatt/timestamp",
                "value_template"        => "{{ as_datetime(value) }}",
                "icon"                  => "mdi:clock",
                "device"                => $this->getDeviceInfo(first: true),
                "origin"                => array(
                    "name"          => "nrg",
                    "sw_version"    => "0.x",
                    "url"           => "https://gitlab.com/jeroenrnl/nrg"
                ),
                "platform"              => "mqtt"
            ), array(
                "unique_id"             => "total_energy",
                "device_class"          => "energy",
                "name"                  => "Solar Energy Generated",
                "state_topic"           => "energy/solar/growatt/total_energy",
                "unit_of_measurement"   => "kWh",
                "value_template"        => "{{ value | round(3) }}",
                "icon"                  => "mdi:solar-power-variant",
                "state_class"           => "total_increasing",
                "device"                => $this->getDeviceInfo(),
                "platform"              => "mqtt"
            ), array(
                "unique_id"             => "today_energy",
                "device_class"          => "energy",
                "name"                  => "Solar Energy Generated Today",
                "state_topic"           => "energy/solar/growatt/today_energy",
                "unit_of_measurement"   => "kWh",
                "value_template"        => "{{ value | round(3) }}",
                "icon"                  => "mdi:solar-power-variant",
                "state_class"           => "total_increasing",
                "device"                => $this->getDeviceInfo(),
                "platform"              => "mqtt"
            ), array(
                "unique_id"             => "pv_power",
                "device_class"          => "power",
                "name"                  => "PV Solar Power",
                "state_topic"           => "energy/solar/growatt/pv_power",
                "unit_of_measurement"   => "W",
                "value_template"        => "{{ value | round(3) }}",
                "icon"                  => "mdi:solar-power-variant",
                "state_class"           => "measurement",
                "device"                => $this->getDeviceInfo(),
                "platform"              => "mqtt"
            ), array(
                "unique_id"             => "ac_power",
                "device_class"          => "power",
                "name"                  => "AC Solar Power",
                "state_topic"           => "energy/solar/growatt/ac_power",
                "unit_of_measurement"   => "W",
                "value_template"        => "{{ value | round(3) }}",
                "icon"                  => "mdi:solar-power-variant",
                "state_class"           => "measurement",
                "device"                => $this->getDeviceInfo(),
                "platform"              => "mqtt"
            ), array(
                "unique_id"             => "pv_voltage",
                "device_class"          => "voltage",
                "name"                  => "PV Voltage",
                "state_topic"           => "energy/solar/growatt/pv_voltage",
                "unit_of_measurement"   => "V",
                "icon"                  => "mdi:flash-triangle",
                "state_class"           => "measurement",
                "device"                => $this->getDeviceInfo(),
                "platform"              => "mqtt"
            ), array(
                "unique_id"             => "ac_voltage",
                "device_class"          => "voltage",
                "name"                  => "AC Voltage",
                "state_topic"           => "energy/solar/growatt/ac_voltage",
                "unit_of_measurement"   => "V",
                "icon"                  => "mdi:flash-triangle",
                "state_class"           => "measurement",
                "device"                => $this->getDeviceInfo(),
                "platform"              => "mqtt"
            ), array(
                "unique_id"             => "pv_current",
                "device_class"          => "current",
                "name"                  => "PV Current",
                "state_topic"           => "energy/solar/growatt/pv_current",
                "unit_of_measurement"   => "A",
                "icon"                  => "mdi:current-dc",
                "state_class"           => "measurement",
                "device"                => $this->getDeviceInfo(),
                "platform"              => "mqtt"
            ), array(
                "unique_id"             => "ac_current",
                "device_class"          => "current",
                "name"                  => "AC Current",
                "state_topic"           => "energy/solar/growatt/ac_current",
                "unit_of_measurement"   => "A",
                "icon"                  => "mdi:current-ac",
                "state_class"           => "measurement",
                "device"                => $this->getDeviceInfo(),
                "platform"              => "mqtt"
            ), array(
                "unique_id"             => "ac_frequency",
                "device_class"          => "frequency",
                "name"                  => "AC Frequency",
                "state_topic"           => "energy/solar/growatt/ac_frequency",
                "unit_of_measurement"   => "Hz",
                "icon"                  => "mdi:current-ac",
                "state_class"           => "measurement",
                "device"                => $this->getDeviceInfo(),
                "platform"              => "mqtt"
            ), array(
                "unique_id"             => "temperature",
                "device_class"          => "temperature",
                "name"                  => "Temperature",
                "state_topic"           => "energy/solar/growatt/temperature",
                "unit_of_measurement"   => "°C",
                "icon"                  => "mdi:thermometer",
                "state_class"           => "measurement",
                "device"                => $this->getDeviceInfo(),
                "platform"              => "mqtt"
            )
        );
    }

    private function getDeviceInfo(bool $first = true) : array {
        $d = $this->getData();
        $serial = $d["serial"];
        $i = $this->getInverterData($serial);


        $info = array(
            "identifiers"   => $d["serial"],
        );

        if ($first) {
            $info["name"]               = "Growatt " . $serial;
            $info["manufacturer"]       = $i->make;
            $info["model"]              = $i->type;
            $info["serial_number"]      = $serial;
            $info["sw_version"]         = $i->swversion;
            $info["hw_version"]         = $i->hwversion;
        }

        return $info;
    }
}

