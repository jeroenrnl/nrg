<?php
namespace collector\dsmr\repository;

use DateTime;
use DateTimeZone;
use PDO;
use stdClass;

use repositoryInterface;

use db\clause;
use db\db;
use db\insert;
use db\param;
use db\select;
use db\update;

class repository implements repositoryInterface {
    const TABLE="measurement";
    public function __construct() {
    }

    public function load($id) {

    }

    public function save($measurement) {
        $qry=new insert(static::TABLE);

        foreach ($measurement->getData() as $label => $value) {
            $param=new param(":" . $label, $value);
            $qry->addParam($param);
        }
        $qry->execute();
    }

    public function getAll() {

    }

    public function updateFlowData() {
        $subqry = new select(static::TABLE);
        $subqry->addFields(array("datetime"));
        $subqry->addFunction(array("flow" => "((water - (lag(water) over (order by datetime))) * 1000) / (datetime - lag(datetime) over (order by datetime))"));
        $subqry->addGroupBy("datetime");

        $qry = new update(static::TABLE);
        $qry->join(array("m2" => $subqry), "measurement.datetime = m2.datetime");
        $qry->where(new clause("measurement.WaterFlow is null"));

        $qry->addSetFunction("measurement.WaterFlow = m2.flow");

        $qry->execute();
    }

    public function getMonthGas() {
        $function = array(
            "date"  =>  "date(GasM1DateTime)",
            "gas"   =>  "max(GasM1) - min(GasM1)"
        );
        $qry=$this->getAggrQry();
        $qry->addFunction($function);

        $qry->addLimit(30);
        $qry->addOrder("date DESC");
        $qry->addGroupBy("date");
        $result=db::query($qry);

        $measurements=$result->fetchAll(PDO::FETCH_CLASS,"StdClass");
        
        $month = new StdClass();
        $month->name="Gas usage per day";
        $month->data=array();
        $month->days = array();

        foreach (array_reverse($measurements) as $measurement) {
            $month->days[] = $measurement->date;
            $month->data[]=(float) $measurement->gas;
        }
        return $month;
   
         
    }

    public function getYearGas() {
        $function = array(
            "month"  =>  "month(GasM1DateTime)",
            "year"  =>  "year(GasM1DateTime)",
            "gas"   =>  "max(GasM1) - min(GasM1)"
        );

        $qry=$this->getAggrQry();
        $qry->addFunction($function);

        $qry->addLimit(12);
        $qry->addOrder("year DESC, month DESC");
        $qry->addGroupBy("month, year");
        $result=db::query($qry);

        $measurements=$result->fetchAll(PDO::FETCH_CLASS,"StdClass");
        
        $year = new StdClass();
        $year->name="Gas usage per month";
        $year->data=array();
        $year->days = array();

        foreach (array_reverse($measurements) as $measurement) {
            $year->days[] = $measurement->year . "-" . $measurement->month;
            $year->data[]=(float) $measurement->gas;
        }
        return $year;
   
         
    }

    private function getAggrQry() {
        $msm=new select(static::TABLE);
        $msm->addFields(array("GasM1DateTime", "GasM1"));
        $agg_min = new select("aggregate_minute"); 
        $agg_min->addFields(array("GasM1DateTime", "GasM1"));
        $msm->union($agg_min);
        $agg_hour = new select("aggregate_hour"); 
        $agg_hour->addFields(array("GasM1DateTime", "GasM1"));
        $msm->union($agg_hour);
        
        $qry = new select(array("msm" => $msm));
        return $qry;
    }
    public function getLast() {
        $qry=new select(static::TABLE);
        $qry->addLimit(1);
        $qry->addOrder("datetime DESC");
        $result=db::query($qry);
        $measurement = new \collector\dsmr\measurement(new static);

        $measurement->data = $result->fetch(PDO::FETCH_ASSOC);
        return $measurement;
    }

    public function getLastHour() {
        $qry=new select(static::TABLE);
        $qry->addFields(array("datetime", "current", "CurrentL1", "CurrentL2", "CurrentL3", "water", "WaterFlow"));
        $qry->addOrder("datetime");
        $qry->where(new clause("datetime >= DATE_SUB(NOW(), INTERVAL 1 HOUR)"));
        $result=db::query($qry);
        $measurements=$result->fetchAll(PDO::FETCH_ASSOC);

        $currentL1=new StdClass();
        $currentL1->name="Gebruik Fase 1";
        $currentL1->field="CurrentL1";
        $currentL1->data=array();

        $currentL2=new StdClass();
        $currentL2->name="Gebruik Fase 2";
        $currentL2->field="CurrentL2";
        $currentL2->data=array();

        $currentL3=new StdClass();
        $currentL3->name="Gebruik Fase 3";
        $currentL3->field="CurrentL3";
        $currentL2->data=array();
        $currentL3->data=array();

        $current=new StdClass();
        $current->name="Gebruik Totaal";
        $current->field="current";
        $current->data=array();
        
        $water=new StdClass();
        $water->name="Water usage";
        $water->field="water";
        $water->data=array();

        $waterflow=new StdClass();
        $waterflow->name="Water flow";
        $waterflow->field="WaterFlow";
        $waterflow->data=array();

        $fields = [ $currentL1, $currentL2, $currentL3, $current, $water, $waterflow ];
        foreach ($measurements as $measurement) {
            $date = DateTime::createFromFormat('Y-m-d H:i:s', $measurement["datetime"], new DateTimeZone("UTC"))->getTimestamp()*1000;
            //$date=$measurement->datetime;
            foreach ($fields as $field) {
                $field->data[]=array($date, (float) $measurement[$field->field] * 1000);
            }

         }
         return $fields;
    }
    
    public function getHealth(int $minutes) {
        $qry=new select(self::TABLE);
        $qry->addFunction(array("count" => "count(*)"));
        $qry->where(new clause("datetime > (date_sub(now(), interval " . $minutes . " minute))"));
        return $qry->getCount() !== 0;
    }
}
