#!/usr/bin/php
<?php
require_once("include/include.php");
require_once("vendor/autoload.php");

use \PhpMqtt\Client\MqttClient;
use \PhpMqtt\Client\ConnectionSettings;

use emitter\growattMqtt\emitter;

include("include/mqttconnect.php");

$mqtt = new MqttClient($server, $port, $clientId . "-growatt", $mqtt_version);

$emitter = new emitter($mqtt, $settings, $growattTopic);

$emitter->run();
?>
