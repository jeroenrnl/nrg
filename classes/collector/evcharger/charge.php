<?php
namespace collector\evcharger;

use repositoryInterface;
use DateTime;

class charge {

    private $id = 0;

    public function __construct(
        private repositoryInterface $repo,
        private int                 $session_id,
        private DateTime            $start,
        private float               $start_power,
        private null|DateTime       $end = null,
        private null|float          $end_power = null
    ) {}

    public function save() {
        $this->repo->save($this);
    }

    public function update() {
        $this->repo->update($this);
    }

    public function getData() {
        return array(
            "session_id"    => $this->session_id,
            "start"         => $this->start,
            "end"           => $this->end,
            "start_power"   => $this->start_power,
            "end_power"     => $this->end_power
        );
    }

    public function setId(int $id) {
        $this->id = $id;
    }
    
    public function getId() {
        return $this->id;
    }


    public static function getForSession(repositoryInterface $repo, session $session) {
        return $repo->getForSession($session);
    }

    public function getEndPower() {
        return (float) $this->end_power;
    }

    public function hasEnded() {
        return !is_null($this->end);
    }

    public function end(float $end_power) {
        $repo = new \collector\evcharger\repository\power();
        
        $this->end = new DateTime();
        $this->end_power=$end_power;

        $this->update();
    }

    public static function getCurrent(repositoryInterface $repo) {
        return $repo->getCurrent();
    }

    public static function start(repositoryInterface $repo, float $start_power) {
        $powerRepo = new \collector\evcharger\repository\power();
        $sessionRepo = new \collector\evcharger\repository\session();
        $session = session::getCurrent($sessionRepo);
        if (!$session) {
            $session = session::start(new \collector\evcharger\repository\session(), "UNKNOWN");
        }

        $start = new DateTime();

        $charge = new self($repo, $session->getId(), $start, $start_power);
        $repo->save($charge);

        return $charge;

    }

    public static function getLast(int $count) {
        $chargeRepo = new \collector\evcharger\repository\charge();
        return $chargeRepo->getLast($count);
    }

}
