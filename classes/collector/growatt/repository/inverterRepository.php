<?php
namespace collector\growatt\repository;

use db\clause;
use db\db;
use db\replace;
use db\param;
use db\select;

use collector\growatt\inverter;

use repositoryInterface;

use PDO;
use stdClass;



class inverterRepository implements repositoryInterface {
    const TABLE="growatt_inverter";

    public stdClass $data;

    public function __construct() {
    }

    public function load($serial) {
        $qry=new select(static::TABLE);
        $qry->where(new clause("serial=:serial"));

        $qry->addParam(new param(":serial", $serial, PDO::PARAM_STR));

        try {
            $result = db::query($qry);
        } catch (PDOException $e) {
            log::msg("Lookup failed", log::FATAL, log::DB);
        }

        $results=$result->fetchAll(PDO::FETCH_CLASS, stdClass::class);
        $rows=count($results);

        $this->data=array_pop($results);
    }

    public function save($data) {
        $qry=new replace(static::TABLE);

        foreach ($data as $label => $value) {
            $param=new param(":" . $label, $value, PDO::PARAM_STR);
            $qry->addParam($param);
        }

        $qry->execute();
    }

    public function getAll() {

    }
}

