<?php
namespace collector\evcharger;

use repositoryInterface;
use DateTime;

class event {

    private $datetime;

    public function __construct(private repositoryInterface $repo, private string $event, private string $data) {
        $this->datetime = new DateTime("now");
    }

    public function save() {
        $this->repo->save($this);
    }

    public function getData() {
        return array(
            "timestamp" => $this->datetime,
            "action"    => $this->event,
            "data"      => $this->data
        );
    }
}
