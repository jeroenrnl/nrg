<?php
namespace collector\growatt;

use repositoryInterface;

class inverter {
    private array $data;

    public function __construct(private repositoryInterface $repo) {
    }

    public function processAnnouncement(array $announcement) : void {
        $this->data = array(
            "make"      => $announcement["make"],
            "type"      => $announcement["type"],
            "serial"    => $announcement["serial"],
            "inverter"  => $announcement["ident"],
            "ident"     => $announcement["ident2"],
        );
    }

    public function processConfig(array $config) : void {
        $this->data["hwversion"]      = $config[0x09] ?? "";
        $this->data["device_type"]    = $config[0x14] ?? "";
        $this->data["swversion"]      = $config[0x15] ?? "";
    }

    public function save() : void {
        if (!empty($this->data["serial"])) {
            $this->repo->save($this->data);
        }
    }
}



?>
