<?php
namespace collector\dsmr\obis;

interface obisValueType {

    public function getConvertedValue($value);
    public function getUnit($value);

}
