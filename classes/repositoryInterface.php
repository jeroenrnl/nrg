<?php

interface repositoryInterface {
    public function load($id);
    public function save($data);
    public function getAll();
}

