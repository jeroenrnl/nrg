var meterR=function() {
    function update(data) {
        document.getElementById("meter-T1R").innerHTML=parseFloat(data.T1R).toFixed(3);
        document.getElementById("meter-T2R").innerHTML=parseFloat(data.T2R).toFixed(3);
        if(data.tariff.charCodeAt(1)==1 || data.tariff=="1") {
            document.getElementById("meterR-tariff-arrow").innerHTML="&#8598;"
        } else if(data.tariff.charCodeAt(1)==2 || data.tariff=="2") {
            document.getElementById("meterR-tariff-arrow").innerHTML="&#8601;"
        } else {
            document.getElementById("meterR-tariff-arrow").innerHTML="&#8604;"
        }
    }
    
    return {
        update:update
    };
}();

