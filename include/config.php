<?php
    define('LOG_SEVERITY', log::NONE);
    define('LOG_SUBJECT', log::NONE);
    define('LOG_ALWAYS', log::FATAL);

    date_default_timezone_set(getenv("TZ") ?? "UTC");
?>
