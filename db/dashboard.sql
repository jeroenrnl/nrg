CREATE TABLE `dashboard` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(30) NOT NULL,
  `title` char(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `dashboard_widgets` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(30) NOT NULL,
  `dashboard` bigint(20) unsigned NOT NULL,
  `row` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `dashboard_widgets_parameters` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `db_widget_id` bigint(20) unsigned DEFAULT NULL,
  `parameter` varchar(30) DEFAULT NULL,
  `value` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

