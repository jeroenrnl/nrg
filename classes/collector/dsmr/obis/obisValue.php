<?php
namespace collector\dsmr\obis;

class obisValue {
    private $id;
    private $name;
    private $valueType;
    private $desc;

    private $rawvalue;
    private $value;
    private $unit;

    public function __construct($id, $name, obisValueType $type, $desc) {
        $this->id=$id;
        $this->name=$name;
        $this->valueType=$type;
        $this->desc=$desc;
    }

    public function getId() {
        return $this->id;
    }

    public function setValue($value) {
        $this->rawvalue=$value;
        $this->value=$this->valueType->getConvertedValue($value);
        $this->unit=$this->valueType->getUnit($value);
    }

    public function getName() {
        return $this->name;
    }

    public function getValue() {
        return $this->value;
    }

    public function getUnit() {
        return $this->unit;
    }

    public function getDesc() {
        return $this->desc;
    }


}
