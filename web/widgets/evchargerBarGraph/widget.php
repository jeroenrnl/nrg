<?php
class evchargerBarGraph extends widget implements widgetInterface {
    const NAME="evchargerBarGraph";

    protected $files=array(
        "css" => "widget.css",
        "js" => array(
                "widget.js",
                "https://code.jquery.com/jquery-1.9.1.js",
                "https://code.highcharts.com/highcharts.js",
                "https://code.highcharts.com/highcharts-more.js"
            ),
        "html" => "widget.html"
    );

    protected $parameters=array(
    );

    public function getUpdate() {
        return array();
    }

}
