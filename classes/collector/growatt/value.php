<?php
namespace collector\growatt;

class value {
    public $name;
    public $bytes;
    public $unit;
    public $scale;
    public $desc;

    public $value;

    private $remaining;

    public function __construct($name, $unit = null, $bytes = 2, $scale = 1, $desc = "Unknown") {
        $this->name=$name;
        $this->unit=$unit;
        $this->bytes=$bytes;
        $this->scale=$scale;
        $this->desc=$desc;

    }

    public function getFromBuffer($buffer) {
        $pack = $this->bytes == 2 ? "n" : "N";
        $dataArray = unpack($pack, substr($buffer, 0, $this->bytes));
        $data=array_pop($dataArray);
        $this->value = $data / (10 ** $this->scale);

        $this->remaining = substr($buffer, $this->bytes);

        return $this->value;

    }

    public function getRemaining() {
        return $this->remaining;
    }

}



?>
