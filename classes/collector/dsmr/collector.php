<?php
namespace collector\dsmr;

use aggregate;
use collector\dsmr\repository\repository;
use collector\dsmr\repository\aggregate\hour as aggregateHour;
use collector\dsmr\repository\aggregate\minute as aggregateMinute;

class collector {
    private $serial;

    public function __construct($serial) {
        $this->serial=$serial;
    }

    /**
     * This function reads bytes from a serial port
     * For now, all settings are hardcoded
     * The data from the serial port is chunked into lines
     */
    private function readSerial() {
        $this->serial->deviceSet("/dev/ttyUSB0");
        $this->serial->confBaudRate(115200);
        $this->serial->confParity("none");
        $this->serial->confCharacterlength(8);
        $this->serial->confStopBits(1);
        $this->serial->confFlowControl("none");

        $this->serial->deviceOpen();

        //Flush buffer
        $data="!";
        while (substr($data,1)=="!") {
            syslog(LOG_DEBUG, "Syncing data stream...");
            $data=$this->serial->readPort(5);
        }
        syslog(LOG_INFO, "Starting data readout");

        while (true) {
        $data=$this->serial->readPort(0);
            if (!empty($data)) {
                $lines=explode("\n", $data);
                foreach ($lines as $line) {
                    yield trim(str_replace(array("\n", "\r"), "", $line));
                }
            }
            sleep(5);
        }
    }

    /**
     * Reads lines from @see readSerial() and builds messages out of them
     */
    private function msg() {
        $msg=array();
        foreach ($this->readSerial() as $line) {
            if (strpos($line, "/") === 0) {
                $msg=array();
            }

            if (strpos($line, "(") === 0) {
                $msg[key($msg)].=$line;
            } else {
                $msg[]=$line;
            }

            if (strpos($line, "!") === 0) {
                yield $msg;
                $msg=array();
            }
        }
    }

    /**
     * Runs the data collector service
     */
    public function run() {
        openlog("nrg", LOG_PID | LOG_PERROR, LOG_LOCAL0);
        syslog(LOG_INFO, "nrg collector starting");
        $time=time();
        foreach ($this->msg() as $msg) {
            $telegram=new telegram($msg);
            if ($telegram->isValid()) {
                $measurement=new measurement(new repository());
                $measurement->readFromTelegram($telegram);
                $measurement->save();
            } else {
                syslog(LOG_DEBUG, "Invalid telegram");
            }

            // Every hour...
            if (time() - $time > 3600) {
                syslog(LOG_INFO, "Hourly aggration of nrg levels");
                $time=time();
                $aggregate=new aggregate(new aggregateMinute);
                $aggregate->create(new repository());

                // Saturday around midnight
                if ((date("N") == 6) && (date("G") == 0)) {
                    syslog(LOG_INFO, "Weekly aggration of nrg levels");
                    $aggregate=new aggregate(new aggregateHour);
                    $aggregate->create(new aggregateMinute);
                }

            }
        }
        syslog(LOG_INFO, "nrg collector stopping");
        closelog();
    }
}


