<?php
namespace collector\evcharger;

use repositoryInterface;
use DateTime;

class power {

    private $datetime;

    public function __construct(private repositoryInterface $repo, private float $power, private float $total) {
        $this->datetime = new DateTime("now");
    }

    public function save() {
        $this->repo->save($this);
    }

    public function getData() {
        return array(
            "timestamp" => $this->datetime,
            "power"     => $this->power,
            "total"     => $this->total
        );
    }

    public static function getMaxTotal(repositoryInterface $repo) {
        return (int) $repo->getMaxTotal();
    }
}
