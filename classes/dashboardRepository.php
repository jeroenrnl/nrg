<?php

use db\clause;
use db\db;
use db\param;
use db\select;

class DashboardRepository implements repositoryInterface {
    const TABLE="dashboard";
    private $id;


    public function __construct() {
    }

    public function load($id) {

    }

    public function save($dashboard) {
        return false;
        //$qry=new insert(static::TABLE);

        //foreach ($measurement->getData() as $label => $value) {
        //    $param=new param(":" . $label, $value);
        //    $qry->addParam($param);
       // }
        //$qry->execute();
    }

    public function getAll() {
        $qry=new select(static::TABLE);
        $result=db::query($qry);
        return $result->fetchAll(PDO::FETCH_CLASS, "dashboard", array(new static));
    }

    public function getById($id) {
        $qry=new select(static::TABLE);
        $qry->addLimit(1);
        $qry->where(new clause("id = :id"));
        $qry->addParam(new param(":id", $id, PDO::PARAM_INT));
        $result=db::query($qry);
        return $result->fetchObject("dashboard", array(new static));
    }

    public function getByName($name) {
        $qry=new select(static::TABLE);
        $qry->addLimit(1);
        $qry->where(new clause("name = :name"));
        $qry->addParam(new param(":name", $name, PDO::PARAM_STR));
        $result=db::query($qry);
        return $result->fetchObject("dashboard", array(new static));
    }

    public function getWidgets($id) {
        $qry=new select("dashboard_widgets");
        $qry->where(new clause("dashboard = :dbid"));
        $qry->addParam(new param(":dbid", $id, PDO::PARAM_INT));
        $qry->addOrder("row, id");
        $result=db::query($qry);
        $widgets=array();
        $row=0;
        foreach ($result->fetchAll() as $w) {
            $wrow = $w["row"];
            if ($wrow != $row) {
                $widgets[$wrow] = array();
                $row = $wrow;
            }
            $widgets[$row][]=new $w["name"]($w["id"], $this);
        }
        return $widgets;
    }

    public function getWidgetParameters($widgetId) {
        $qry=new select("dashboard_widgets_parameters");
        $qry->where(new clause("db_widget_id = :dbwid"));
        $qry->addParam(new param(":dbwid", $widgetId, PDO::PARAM_INT));
        $result=db::query($qry);
        $params=array();
        foreach ($result->fetchAll() as $p) {
            $params[$p["parameter"]]=$p["value"];
        }
        return $params;
    }

}
