var growattDayGraph=function() {
    function graph(container, title) {
        var data=[]
        Highcharts.setOptions({
            global: {
                getTimezoneOffset: function (timestamp) {
                    d = new Date();
                    timezoneOffset =  d.getTimezoneOffset()

                    return timezoneOffset;
                }
            }
        });
        $.ajax({
            url: "getJSON.php?data=lastGrowatt",
            async: false,
            dataType: "json",
            useUTC: true,
            success: function(json) {
                data=json;
            }
        });
        return new Highcharts.Chart({
            chart: {
                type: 'spline',
                renderTo: container,
                useUTC: false 
            },
            title: {
                text: title
            },
            xAxis: {
                type: 'datetime'
            },
            yAxis: {
                min: 0
            },
            plotOptions: {
                spline: {
                    marker: {
                        enabled: false
                    }
                }
            },
            series: data
        });
    }

    function update(graph, fields, data) {
        var graph1 = graph.series[0];
        var time=new Date(data.datetime.replace(" ", "T")).getTime() - (new Date().getTimezoneOffset() * 60000);

        graph1.addPoint([time,parseFloat(data[fields[0]])]);

        while(graph1.points[0].x < (time - (60 * 60 * 24 * 2 * 1000))) {
            graph1.points[0].remove();
        }
    }

    return {
        graph:graph,
        update:update
    }
}();
