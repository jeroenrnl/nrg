var hourGraph=function() {
    function graph(container, fields, min, title) {
        var jsonData=[]
        $.ajax({
            url: "getJSON.php?data=lasthour",
            async: false,
            dataType: "json",
            success: function(json) {
                jsonData=json;
            }
        });

        var data = [];
        jsonData.forEach(function(json) {
            if (fields.includes(json.field)) {
                data.push(json);
            }
        });

        return new Highcharts.Chart({
            chart: {
                type: 'spline',
                renderTo: container,
                useUTC: false 
            },
            title: {
                text: title
            },
            xAxis: {
                type: 'datetime'
            },
            yAxis: {
                min: min
            },
            plotOptions: {
                spline: {
                    marker: {
                        enabled: false
                    }
                }
            },
            series: data
        });
    }

    function update(graph, fields, data) {
        var graphs = [];
        fields.forEach(function(field, nr) {
            graph[nr] = graph.series[nr];
            var time=new Date(data.datetime.replace(" ", "T")).getTime() - (new Date().getTimezoneOffset() * 60000);

            graph[nr].addPoint([time,parseFloat(data[field])*1000]);

            var first=graph[nr].points[0];
            if(first.x < (time - (60 * 60 * 1000))) {
                first.remove();
            }
        });
    }

    return {
        graph:graph,
        update:update
    }
}();
