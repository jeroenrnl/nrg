<?php
namespace collector\dsmr\repository\aggregate;

use repositoryInterface;

class hour extends aggregateRepository implements repositoryInterface {
    const TABLE="aggregate_hour";

    public function create($repository) {
        parent::createAggregate($repository, [ "hour" => "date_format(datetime, '%Y-%m-%d %H:00:00')" ]);
    }
}

