#!/usr/bin/php
<?php
require_once("include/include.php");

use collector\growatt\collector;

pcntl_async_signals(true);
pcntl_signal(SIGINT, function() { collector::signalHandler(); });
pcntl_signal(SIGTERM, function() { collector::signalHandler(); });
pcntl_signal(SIGHUP, function() { collector::signalHandler(); });

set_time_limit(0);

$address = '0.0.0.0';
$port = 5279;

$gwc = new collector($address, $port);
$gwc->handleIncoming();

?>
