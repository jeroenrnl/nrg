var gaugeAmp=function() {
    function gauge(container, title, max) {
        var gaugeTitle = "Ampere";
        return new Highcharts.Chart({
            chart: {
                renderTo: container,
                type: 'gauge',
                plotBackgroundColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                    stops: [
                        [0, '#FFF4C6'],
                        [0.3, '#FFFFFF'],
                        [1, '#FFF4C6']
                    ]
                },
                plotBackgroundImage: null,
                plotBorderWidth: 1,
                plotShadow: true,
                height: 150
            },

            title: {
                text: title
            },
            pane: {
                startAngle: -45,
                endAngle: 45,
                background: null,
                center: ['50%','165%'],
                size: 250
            },

            // the value axis
            yAxis: {
                min: 0,
                max: max,

                tickPosition: 'outside',
                minorTickPosition: 'outside',
                labels: {
                    rotation: 'auto',
                    distance: 15 
                },
                title: {
                    step: 5,
                    text: gaugeTitle
                },
                plotBands: [{
                    from: 20,
                    to: 25,
                    innerRadius: '100%',
                    outerRadius: '105%',
                    color: '#DF5353' // red
                }]
            },

            series: [{
                name: title,
                data: [0],
                tooltip: {
                    valueSuffix: gaugeTitle
                }, 
            }],

            plotOptions: {
                gauge: {
                    dataLabels: {
                        y: -105,
                        style: {
                            fontSize: "12px"
                        },
                        backgroundColor: "white"
                    },
                    dial: {
                        radius: '100%'
                    }
                }

            }
        });
    }

    function update(widget, field, data) {
        var gauge = widget.series[0].points[0];
        gauge.update(parseFloat(data[field]));
    }

    return {
        gauge:gauge,
        update:update
    };
}();
