<?php
namespace collector\evcharger\repository;

use DateTime;
use repositoryInterface;
use collector\evcharger\session;
use db\db;
use db\select;
use db\clause;
use db\param;
use PDO;
use StdClass;

class charge extends repository implements repositoryInterface {
    const TABLE="evcharger_charge";
    protected static $OBJECT=\collector\evcharger\charge::class;

    public function getForSession(session $session) {
        $qry = new select(static::TABLE);
        $qry->where(new clause("session_id=:sessionId"));
        $qry->addParam(new param(":sessionId", (int) $session->getId(), PDO::PARAM_INT));
        $result=db::query($qry);
        $rows = $result->fetchAll(PDO::FETCH_OBJ);
        $charges = array();
        foreach ($rows as $row) {
            $start = new DateTime($row->start);
            $end = empty($row->end) ? null : new DateTime($row->end);
            $charge = new static::$OBJECT($this, $row->session_id, $start, $row->start_power, $end, $row->end_power);
            $charge->setId($row->id);
            $charges[] = $charge;
        }
        return $charges;
    }

    public function getCurrent() {
        $qry = new select(static::TABLE);
        $qry->where(new clause("end IS NULL"));
        $qry->addLimit(1);
        $result=db::query($qry);
        $row = $result->fetch(PDO::FETCH_OBJ);
        if ($row) {
            $start = new DateTime($row->start);
            $charge = new static::$OBJECT($this, $row->session_id, $start, $row->start_power, null, null, null);
            $charge->setId($row->id);
        }
        return $charge ?? null;
    }

    public function getLast(int $count) {
        $qry = new select(static::TABLE);
        $qry->addFunction(array(
            "date"  => "date(start)",
            "power" => "sum(end_power - start_power)"

        ));
        $qry->addGroupBy("date");
        $qry->addLimit($count);
        $qry->addOrder("date DESC");

        $reverse = new select(array("month" => $qry));
        $reverse->addOrder("date");
        $result=db::query($reverse);
        $measurements=$result->fetchAll(PDO::FETCH_CLASS,"StdClass");

        $month = new StdClass();
        $month->name="Charged per day";
        $month->data=array();
        $month->days = array();

        foreach ($measurements as $measurement) {
            $month->days[] = $measurement->date;
            $month->data[]=(float) $measurement->power;
        }
        return $month;

    }
}

