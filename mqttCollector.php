#!/usr/bin/php
<?php
require_once("include/include.php");
require_once("vendor/autoload.php");

use \PhpMqtt\Client\MqttClient;
use \PhpMqtt\Client\ConnectionSettings;

use collector\dsmrMqtt\collector;

include("include/mqttconnect.php");

$mqtt = new MqttClient($server, $port, $clientId, $mqtt_version);

$collector=new collector($mqtt, $settings, $dsmrTopic);

$collector->run();

?>
