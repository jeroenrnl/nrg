function graph(container, title) {
    var data=[]
    $.ajax({
        url: "getJSON.php?data=lasthour",
        async: false,
        dataType: "json",
        success: function(json) {
            data=json;
        }
    });
    return new Highcharts.Chart({
        chart: {
            type: 'spline',
            renderTo: container,
            useUTC: false 
        },
        title: {
            text: title
        },
        xAxis: {
            type: 'datetime'
        },
        yAxis: {
            min: 0
        },
        plotOptions: {
            spline: {
                marker: {
                    enabled: false
                }
            }
        },
        series: data
    });
}
