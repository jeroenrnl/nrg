<?php
namespace collector\dsmr\repository\aggregate;

use repositoryInterface;

class minute extends aggregateRepository implements repositoryInterface {
    const TABLE="aggregate_minute";

    public function create($repository) {
        parent::createAggregate($repository, [ "minute" => "date_format(datetime, '%Y-%m-%d %H:%i:00')" ]);
    }
}

