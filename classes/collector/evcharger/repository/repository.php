<?php
namespace collector\evcharger\repository;

use db\insert;
use db\param;
use db\update;
use db\clause;

use collector\evcharger\event;
use repositoryInterface;

use PDO;
use DateTime;


abstract class repository implements repositoryInterface {

    public function __construct() {
    }

    public function save($data) {
        $qry=new insert(static::TABLE);

        foreach ($data->getData() as $label => $value) {
            if ($value instanceof DateTime) {
                $value=$value->format("Y-m-d H:i:s");
            }

            $param=new param(":" . $label, $value, PDO::PARAM_STR);
            $qry->addParam($param);
        }
        $id = $qry->execute();
        if ($id && method_exists($data, "setId")) {
            $data->setId($id);
        }
    }

    public function update($data) {
        $qry=new update(static::TABLE);

        foreach ($data->getData() as $label => $value) {
            if ($value instanceof DateTime) {
                $value=$value->format("Y-m-d H:i:s");
            }

            $param=new param(":" . $label, $value, PDO::PARAM_STR);
            $qry->addParam($param);
            $qry->addSet($label, $label);
        }
        $qry->where(new clause("id=:id"));
        $qry->addParam(new param(":id", $data->getId(), PDO::PARAM_INT));
        $qry->execute();
    }

    public function load($id) {

    }

    public function getAll() {

    }

    public function getById(int $id) {
        $qry=new select(static::TABLE);
        $qry->addLimit(1);
        $qry->where(new clause("id = :id"));
        $qry->addParam(new param(":id", $id, PDO::PARAM_INT));
        $result=db::query($qry);
        return $result->fetchObject(static::OBJECT, array(new static));
    }
}

