var growattBarGraph=function() {
    function graph(container, title) {
        var data=[]
        $.ajax({
            url: "getJSON.php?data=monthGrowatt",
            async: false,
            dataType: "json",
            success: function(json) {
                data=json;
            }
        });
        return new Highcharts.Chart({
            chart: {
                type: 'column',
                renderTo: container,
                useUTC: false 
            },
            title: {
                text: title
            },
            xAxis: {
                categories: data.days
            },
            yAxis: {
                min: 0
            },
            plotOptions: {
            },
            series: [{
                name: "Opbrengst (kWh)",
                data: data.data 
            }] 
        });
    }

    return {
        graph:graph,
    }
}();
