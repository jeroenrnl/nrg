<?php
namespace collector\evcharger\repository;

use repositoryInterface;
use db\db;
use db\select;
use PDO;

class power extends repository implements repositoryInterface {
    const TABLE="evcharger_power";

    public function getMaxTotal() {
        $qry = new select(static::TABLE);
        $qry->addFunction(["max" => "max(total)"]);
        $result = db::query($qry);
        return $result->fetch(PDO::FETCH_BOTH)[0];
    }
}

