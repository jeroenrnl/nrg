<?php
namespace collector\dsmrMqtt;

use DateTime;
use collector\dsmr\repository as repositoryInterface;
use collector\dsmr\measurement as dsmrMeasurement;

class measurement extends dsmrMeasurement {

    public function readFromMQTT(array $mqttData) {
        $this->data=$mqttData;
    }
}
