<?php
namespace collector\dsmrMqtt;

require_once("vendor/autoload.php");

use aggregate;
use collector\dsmr\repository\repository;
use collector\dsmr\repository\aggregate\hour as aggregateHour;
use collector\dsmr\repository\aggregate\minute as aggregateMinute;

use DateTime;
use DateTimeZone;
use Exception;

use \PhpMqtt\Client\MqttClient;
use \PhpMqtt\Client\ConnectionSettings;


class collector {

    private array $data = array();
    private int $time = 0;

    public function __construct(
        private MqttClient $mqtt, 
        private ConnectionSettings $settings,
        private string $topic
    ) {}

    /**
     */
    public function readMQTT() {
        $this->mqtt->subscribe($this->topic . "/#", function ($topic, $message) {
            $this->process($topic, $message);
        }, 0);

        $this->mqtt->loop(true);
    }

    /**
     * Runs the data collector service
     */
    public function run() : void {
        openlog("nrg", LOG_PID | LOG_PERROR, LOG_LOCAL0);
        syslog(LOG_INFO, "nrg collector starting");
        $cleanSession = false;
        $this->mqtt->connect($this->settings, $cleanSession);
        $this->time=time();
        syslog(LOG_INFO, "MQTT Connected");

        $this->readMQTT();

        syslog(LOG_INFO, "nrg collector stopping");
        closelog();
    }

    private function process(string $topic, string $message) : void {
        switch (substr($topic, strlen($this->topic) + 1)) {
            case "equipment_id":
                $this->data["eid"] = $this->getConvertedValue($message);
                break;
            case "gas_equipment_id":
                $this->data["EidM1"] = $this->getConvertedValue($message);
                break;
            case "p1_version":
                $this->data["version"] = $this->getConvertedValue($message);
                break;
            case "gas_delivered_timestamp":
                $data["GasM1DateTime"] = $this->getConvertedTimestamp($message)->format("Y-m-d H:i:s");
                break;
            case "gas_delivered":
                $data["GasM1"] = (float) $message;
                $data["GasM1_unit"] = "m3";
                break;
            case "water":
                $this->data["water"] = (float) $message;
                $this->data["water_unit"] = "m3";
                break;
            case "all":
                $data=$this->decodeJSONdata($message);
                $measurement=new measurement(new repository());
                $measurement->readFromMQTT($data);
                $measurement->save();
                $this->doAggregate();
                break;
            default:
                syslog(LOG_DEBUG, $topic . ": " . $message);
                break; 
        }
    }

    private function decodeJSONdata(string $json) : array {
        $data = $this->data;
        $rcvData = json_decode($json, true);

        foreach(json_decode($json, true) as $topic => $message) {
            switch($topic) {
                case "timestamp":
                    $data["datetime"] = $this->getConvertedTimestamp($message)->format("Y-m-d H:i:s");
                    break;
                case "gas_ts":
                    $data["GasM1DateTime"] = $this->getConvertedTimestamp($message)->format("Y-m-d H:i:s");
                    break;
                case "gas":
                    $data["GasM1"] = (float) $message;
                    $data["GasM1_unit"] = "m3";
                    break;
                case "energy_delivered_tariff1":
                    $data["T1"] = (float) $message;
                    $data["T1_unit"] = "kWh";
                    break;
                case "energy_delivered_tariff2":
                    $data["T2"] = (float) $message;
                    $data["T2_unit"] = "kWh";
                    break;
                case "energy_returned_tariff1":
                    $data["T1R"] = (float) $message;
                    $data["T1R_unit"] = "kWh";
                    break;
                case "energy_returned_tariff2":
                    $data["T2R"] = (float) $message;
                    $data["T2R_unit"] = "kWh";
                    break;
                case "electricity_tariff":
                    $data["tariff"] = (int) $message;
                    break;
                case "current_l1":
                    $data["CurrentAmpL1"] = (float) $message;
                    $data["CurrentAmpL1_unit"] = "A";
                    break;
                case "current_l2":
                    $data["CurrentAmpL2"] = (float) $message;
                    $data["CurrentAmpL2_unit"] = "A";
                    break;
                case "current_l3":
                    $data["CurrentAmpL3"] = (float) $message;
                    $data["CurrentAmpL3_unit"] = "A";
                    break;
                case "power_delivered":
                    $data["Current"] = (float) $message;
                    $data["Current_unit"] = "kW";
                    break;
                case "power_delivered_l1":
                    $data["CurrentL1"] = (float) $message;
                    $data["CurrentL1_unit"] = "kW";
                    break;
                case "power_delivered_l2":
                    $data["CurrentL2"] = (float) $message;
                    $data["CurrentL2_unit"] = "kW";
                    break;
                case "power_delivered_l3":
                    $data["CurrentL3"] = (float) $message;
                    $data["CurrentL3_unit"] = "kW";
                    break;
                case "power_returned":
                    $data["CurrentR"] = (float) $message;
                    $data["CurrentR_unit"] = "kW";
                    break;
                case "power_returned_l1":
                    $data["CurrentRL1"] = (float) $message;
                    $data["CurrentRL1_unit"] = "kW";
                    break;
                case "power_returned_l2":
                    $data["CurrentRL2"] = (float) $message;
                    $data["CurrentRL2_unit"] = "kW";
                    break;
                case "power_returned_l3":
                    $data["CurrentRL3"] = (float) $message;
                    $data["CurrentRL3_unit"] = "kW";
                    break;
                case "water":
                    $data["water"] = (float) $message;
                    $data["water_unit"] = "m3";
                    break;
                default:
                    syslog(LOG_DEBUG, "(JSON)" . $topic . ": " . $message);
                    break; 
            }
        }
        return $data;
    }

    private function doAggregate() : void {
        // Every hour...
        if (time() - $this->time > 3600) {
            syslog(LOG_INFO, "Hourly aggration of nrg levels");
            $this->time=time();
            $aggregate=new aggregate(new aggregateMinute);
            $aggregate->create(new repository());

            // Saturday around midnight
            if ((date("N") == 6) && (date("G") == 0)) {
                syslog(LOG_INFO, "Weekly aggration of nrg levels");
                $aggregate=new aggregate(new aggregateHour);
                $aggregate->create(new aggregateMinute);
            }

        }
    }

    public function getConvertedValue($data) : string {
        $value="";
        $hexes=str_split($data, 2);
        foreach ($hexes as $hex) {
            $value.=chr(hexdec($hex));
        }
        return $value;
    }
    
    public function getConvertedTimeStamp($data) : DateTime {
        if (substr($data, -1 == "W")) {
            $data=substr($data, 0, -1);
            $tz=new dateTimeZone("CET");
        } else if (substr($data, -1 == "S")) {
            $data=substr($data, 0, -1);
            $tz=new dateTimeZone("CEST");
        } else {
            throw new Exception("Incorrect data format summer/winter time");
        }

        $time=DateTime::createFromFormat("ymdGis", $data);
        if ($time===false) {
            throw new Exception("Error in date/time: " . DateTime::getLastErrors());
        }
        return $time;
    }


}


