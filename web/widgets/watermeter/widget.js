var watermeter=function() {
    function update(data) {
        document.getElementById("watermeter").innerHTML=parseFloat(data.water).toFixed(3);
    }
    
    return {
        update:update
    };
}();

