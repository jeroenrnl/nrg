<?php
namespace collector\dsmr;

use DateTime;
use Exception;

use collector\dsmr\obis\obis;

class telegram {
    private $msg;
    private $timestamp;

    private $identification;
    private $data=array();

    private $crc;

    private $valid=true;

    public function __construct($msg) {
        $this->timestamp=new DateTime();
        $this->msg=$msg;
        try {
            $this->process();
        } catch (Exception $e) {
            echo "WARNING: " . $e->getMessage();
            $this->valid=false;
        }
    }

    public function isValid() {
        // For now, a telegram is valid if it has an identifier, a CRC and at least 5 OBIS lines
        return ($this->valid && !empty($this->identification) && !empty($this->crc) && count($this->data) >= 5);
    }

    public function getId() {
        return $this->identification;
    }

    public function getTimestamp() {
        return $this->timestamp;
    }

    public function getData() {
        return $this->data;
    }

    private function process() {
        foreach ($this->msg as $line) {
            $this->processLine($line);
        }
    }

    private function processLine($line) {
        if (empty($line)) {
            return;
        }

        if (strpos($line, "/") === 0) {
            $this->identification=substr($line, 1);
        } else if (strpos($line, "!") === 0) {
            $this->crc=substr($line, 1);
        } else {
            $obis=new obis($line);
            if ($obis->isValid()) {
                $this->data[]=$obis;
            }
        }
    }
}




