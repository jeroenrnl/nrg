<?php
namespace collector\dsmr;

use DateTime;
use repositoryInterface;

class measurement {
    private $repo;
    public $data=array();

    public function __construct(repositoryInterface $repo) {
        $this->repo=$repo;
    }

    public function readFromTelegram(telegram $telegram) {
        $data=$telegram->getData();

        foreach ($data as $obis) {
            $name=$obis->getName();
            $value=$obis->getData()->getValue();
            $unit=$obis->getData()->getUnit();

            if ($value instanceof DateTime) {
                $value=$value->format("Y-m-d H:i:s");
            }

            $this->data[$name]=$value;


            if (!is_null($unit)) {
                $this->data[$name . "_unit"] = $unit;
            }

            if ($name=="GasM1") {
                $this->data["GasM1DateTime"]=$value[0]->format("Y-m-d H:i:s");
                $this->data["GasM1"]=$value[1];
                $this->data["GasM1_unit"]=$this->data["GasM1_unit"][1];
            }



        }

    }

    public function getData() {
        return $this->data;
    }

    public function save() {
        $this->repo->save($this);
        $this->repo->updateFlowData();
    }

    public function getLast() {
        return $this->repo->getLast();
    }

    public function getMonthGas() {
        return $this->repo->getMonthGas();
    }

    public function getYearGas() {
        return $this->repo->getYearGas();
    }

    public function getLastHour() {
        return $this->repo->getLastHour();
    }


}
