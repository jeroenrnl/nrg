<?php
class gasBarGraph extends widget implements widgetInterface {
    const NAME="gasBarGraph";

    protected $files=array(
        "css" => "widget.css",
        "js" => array(
                "widget.js",
                "https://code.jquery.com/jquery-1.9.1.js",
                "https://code.highcharts.com/highcharts.js",
                "https://code.highcharts.com/highcharts-more.js"
            ),
        "html" => "widget.html"
    );

    protected $parameters=array(
        "title",
        "period"
    );

    public function getUpdate() {
        return array();
    }

}
