<?php
class hourGraph extends widget implements widgetInterface {
    const NAME="hourGraph";

    protected $files=array(
        "css" => "widget.css",
        "js" => array(
                "widget.js",
                "https://code.jquery.com/jquery-1.9.1.js",
                "https://code.highcharts.com/highcharts.js",
                "https://code.highcharts.com/highcharts-more.js"
            ),
        "html" => "widget.html"
    );

    protected $parameters=array(
        "field1",
        "field2",
        "field3",
        "field4",
        "min",
        "max",
        "title"
    );

    public function getInit() {
        $init = "fields[" . $this->getId() . "] = [\n";
        $init .= $this->getFields();
        $init .= "\n];\n";

        return $init;
    }

    public function getUpdate() {
        return array("current" => "hourGraph.update(" . $this->getName() .
            ", [ " . $this->getFields() . "], data)");
    }

    private function getFields() {
        $fields = array();
        foreach ($this->parameters as $param) {
            if (substr($param, 0, 5) == "field") {
                if(!empty($this->getParam($param))) {
                    $fields[] = "\"" . $this->getParam($param) . "\"";
                }
            }
        }
        return implode(", ", $fields);
    }

}
