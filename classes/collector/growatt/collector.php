<?php

namespace collector\growatt;

use DateTime;

use aggregate;
use collector\growatt\repository\repository;
use collector\growatt\repository\inverterRepository;
use collector\growatt\repository\aggregate\hour as aggregateHour;
use collector\growatt\repository\aggregate\day as aggregateDay;


define("EWOULDBLOCK", 11);
define("EINPROGRESS", 115);


class collector {
    const WAITING       = 0;
    const OPEN          = 1;
    const QUERY_SENT    = 2;
    const TIME_SET      = 3;
    const QUERY_RCVD    = 4;
    const ANNOUNCED     = 5;
    const DATA_RCVD     = 6;

    private $state = self::WAITING;

    private $socket=null;
    private $listen=null;
    private $pids=array();

    private $fin=false;

    private $data;
    private $config=array();
    private $version=2;

    private $serial="";

    private inverter $inverter;

    public function __construct($bind, $port) {
        openlog("nrg-growatt", LOG_PID | LOG_PERROR, LOG_LOCAL0);
        syslog(LOG_INFO, "nrg growatt collector starting");
        syslog(LOG_INFO, "Listening on " . $bind . ":" . $port);

        if (($sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) === false) {
            syslog(LOG_ERR, "socket_create() failed: reason: " . socket_strerror(socket_last_error()));
            exit(10);
        }

        if (socket_bind($sock, $bind, $port) === false) {
            syslog(LOG_ERR, "socket_bind() failed: reason: " . socket_strerror(socket_last_error($sock)));
            exit(11);
        }

        if (socket_listen($sock, 5) === false) {
            syslog(LOG_ERR, "socket_listen() failed: reason: " . socket_strerror(socket_last_error($sock)));
            exit(12);
        }

        socket_set_nonblock($sock);


        $this->socket = $sock;
    }

    public function run() {
        $time=time();
        $this->inverter = new inverter(new inverterRepository());
        foreach ($this->receive() as $buf) {
            if ($this->fin) {
                break;
            }

            while (strlen($buf) > 0) {
                $msg = message::createFromBuffer($buf);
                $msg->setInverter($this);

                $reply="";

                if ($msg) {
                    $reply=$this->processMsg($msg);
                    $buf = $msg->getRemaining();
                }

                if (!empty($reply)) {
                    socket_write($this->socket, $reply->getMsg(), $reply->getSize());
                    unset($reply);
                }

            }

            if (time() - $time > 3600) {
                $time = time();
                // First day of the month, around midnight
                if (date("j") == 1 && date("G") == 0) {
                    syslog(LOG_INFO, "Monthly aggration of Growatt data");
                    $aggregate=new aggregate(new aggregateHour());
                    $aggregate->create(new repository());
                }
                // January first, around midnight
                if (date("n") == 1 && date("j") == 1 && date("G") == 0) {
                    syslog(LOG_INFO, "Monthly aggration of Growatt data");
                    $aggregate=new aggregate(new aggregateDay());
                    $aggregate->create(new aggregateHour);
                }
            }

                
        }
    }

    public function receive() {
        $lastmsg=time();
        while (!$this->fin && (time() - $lastmsg < 7200)) {
            if (($buf = socket_read($this->socket, 2048, PHP_BINARY_READ))===false) {
                $error = socket_last_error($this->socket);
                if ($error != EWOULDBLOCK && $error != EINPROGRESS) {
                    syslog(LOG_ERR, "socket_read() failed: reason: " . socket_strerror($error));
                    break;
                }
            } else if (strlen($buf) > 0) {
                $lastmsg = time();
                yield $buf;
            }
            sleep(1);
        }
        syslog(LOG_INFO, "Terminating connection.");
        if(time() - $lastmsg >= 7200) {
            syslog(LOG_INFO, "No message received for 2 hours");
        }
    }

    public function handleIncoming() {
        while (!$this->fin) {
            if (!$this->socket instanceof \Socket) {
                break;
            }

            $pid=pcntl_waitpid(0, $status, WNOHANG);
            if($pid > 0) {
                syslog(LOG_INFO, $pid . " has exited with status " . $status . ".");
            }
                
            $listen = array($this->socket);
            $write = null;
            $except = null;
            if (socket_select($listen, $write, $except, 500) < 1) {
                continue;
            }

            if (($msgsock = socket_accept($this->socket)) === false) {
                $error = socket_last_error($this->socket);
                if ($error !=0 && $error != EWOULDBLOCK && $error != EINPROGRESS) {
                    syslog(LOG_ERR, "socket_read() failed: reason: " . socket_strerror($error));
                    break;
                }
                sleep(2);
            }

            socket_set_nonblock($msgsock);
            $pid = pcntl_fork();

            if ($pid == -1) {
                syslog(LOG_CRIT, "Cannot fork, stopping");
               die("could not fork");
            } elseif ($pid) {
                $this->pids[] = $pid;
                syslog(LOG_INFO, "Forked $pid");
            } else {
                $ip="";
                socket_getpeername($msgsock, $ip);
                syslog(LOG_NOTICE, "Incoming connection from " . $ip);
                $this->socket=$msgsock;
                $this->run();
                $linger = array ('l_linger' => 0, 'l_onoff' => 1);
                socket_set_option($this->socket, SOL_SOCKET, SO_LINGER, $linger);
                socket_close($this->socket);
            }
        }
        $linger = array ('l_linger' => 0, 'l_onoff' => 1);
        if(is_resource($this->socket)) {
            socket_set_option($this->socket, SOL_SOCKET, SO_LINGER, $linger);
            socket_close($this->socket);
        }
    }

    private function sendQuery() {
        $this->version =5;
        $request = $this->data["serial"];
        $request .= pack("c*", 0x00, 0x04, 0x00, 0x15);

        $msg = new message();
        $msg->create($this->version, message::QUERY, $request, 0x1, 0x51);

        socket_write($this->socket, $msg->getMsg(), $msg->getSize());

        $this->state=self::QUERY_SENT;

    }

    public function getConfig($item) {
        if (isset($this->config[$item])) {
            return $this->config[$item];
        }
    }

    private function processMsg($msg) {
        $this->data=$msg->decode();
        switch($msg->getType()) {
            case message::ANNOUNCE:
            case message::ANNOUNCE50:
            case message::ANNOUNCE51:
                $reply=$this->processAnnounce($msg);
                break;
            case message::PING:
                $reply=$this->processPing($msg);
                break;
            case message::CONFACK:
            case message::CONFACK50:
                $reply=$this->processConfAck($msg);
                break;
            case message::QUERY:
            case message::QUERY51:
                $reply=$this->processQuery($msg);
                break;
            case message::DATA:
            case message::DATA50:
            case message::DATA51:
                $reply=$this->processData($msg);
                break;
            default:
                $reply=null;
                break;
        }
        return $reply;
    }

    private function processAnnounce($msg) {
        syslog(LOG_NOTICE, "Received announcement from " .
            $this->data["make"] . " " .
            $this->data["type"] .
            " serial: " . $this->data["serial"] .
            ", inverter: " . $this->data["ident"] .
            ", ident: " . $this->data["ident2"]);

        $reply = new message();
        $reply->create($msg->getVersion(), $msg->getType(), pack("C", 0x0), $this->data["init"]);
        $this->version=$msg->getVersion();
        $this->state=self::ANNOUNCED;
        $this->inverter->processAnnouncement($this->data);
        return $reply;
    }

    private function processPing($msg) {
        syslog(LOG_DEBUG, "Received ping from " . $this->data["serial"] . ", sending reply");
        $size = strlen($this->data["serial"]) + 2;
        $this->serial = $this->data["serial"];
        $reply = new message();
        $reply->create($msg->getVersion(), message::PING, $this->data["serial"], $this->data["init"]);
        if ($this->state==self::WAITING) {
            $this->state=self::OPEN;
            $this->sendQuery();
            $this->state==self::QUERY_SENT;
        }
        return $reply;
    }

    private function processConfAck($msg) {
        foreach ($this->data as $cfg) {
            syslog(LOG_DEBUG, "Configuration ack received: " . $cfg->display());
        }
        return null;
    }

    private function processQuery($msg) {
        $config = $msg->decodeQueryReply();
        $this->config[$config->getIndex()] = $config->getValue();
        syslog(LOG_DEBUG, "Configuration received: " . $config->display());
        if ($this->state==self::QUERY_SENT) {
            $this->state=self::TIME_SET;
            return $this->sendConfigDate($msg);
        } else if ($this->state==self::TIME_SET) {
            $this->state=self::QUERY_RCVD;
            return $this->sendConfigInterval($msg);
        }
    }

    private function processData($msg) {
        if ($this->state != self::DATA_RCVD) {
            $this->inverter->processConfig($this->config);
            $this->inverter->save();
        }
        $this->state=self::DATA_RCVD;
        $reply = new message();
        $reply->create($msg->getVersion(), $msg->getType(), pack("C", 0x0), $msg->msgid);
        $measurement = new measurement(new repository());
        $measurement->readFromTelegram($this->data);
        $measurement->save();
        return $reply;
    }

    private function sendConfigInterval($msg) {
        $reply=new message();
        $reply->create($msg->getVersion(), message::CONFIG, $this->serial . pack("C*", 0x0, 0x04, 0x0, 0x01, 0x31));
        return $reply;
    }

    private function sendConfigDate($msg) {
        $reply=new message();
        $datetime = new DateTime();
        $date = $datetime->format("Y-m-d H:i:s");
        $reply->create($msg->getVersion(), message::CONFIG, $this->serial . pack("C*", 0x0, 0x1f, 0x0, strlen($date)) . $date);
        return $reply;
    }

    public function signalHandler($signal) {
        global $sockets;
        switch($signal) {
        case SIGTERM:
        case SIGHUP:
        case SIGINT:
            syslog(LOG_NOTICE, "Signal $signal received");
            foreach ($this->pids as $pid) {
                posix_kill($pid, SIGINT);
            }
            $this->fin=true;
        }
        exit;
    }
}
?>
