<?php

namespace collector\dsmr\repository\aggregate;

use aggregate;

use db\clause;
use db\db;
use db\delete;
use db\insert;
use db\select;

use repositoryInterface;



abstract class aggregateRepository implements repositoryInterface {

    public function __construct() {
    }

    public function load($id) {

    }

    public function save($aggregate) {
        return false;
        //$qry=new insert(static::TABLE);

        //foreach ($measurement->getData() as $label => $value) {
        //    $param=new param(":" . $label, $value);
        //    $qry->addParam($param);
       // }
        //$qry->execute();
    }

    public function getAll() {

    }

    protected function createAggregate(repositoryInterface $from, array $groupby) {
        // Delete old data from the measurement table:
        $delete=new delete($from::TABLE);

        $selectmax=new select(static::TABLE);
        $selectmax->addFunction(array("datetime_max" => "max(datetime)"));
        if (substr($selectmax, -1) == ";") {
            $selectmax=substr($selectmax, 0, -1);
        }
        $delete->where(new clause("datetime <= (" . $selectmax . ")"));

        $delete->execute();


        $select=new select($from::TABLE);

        $select->addFields(array("eid"));

        $key=key($groupby);

        $select->addFunction(array(
            $key => current($groupby),
            "T1"    => "min(T1)",
            "T1R"   => "min(T1R)",
            "T2"    => "min(T2)",
            "T2R"   => "min(T2R)"
        ));

        foreach (array(
            "current",
            "currentR",
            "CurrentAmpL1",
            "CurrentAmpL2",
            "CurrentAmpL3",
            "CurrentL1",
            "CurrentL2",
            "CurrentL3",
            "CurrentRL1",
            "CurrentRL2",
            "CurrentRL3") as $field) {

            $this->addAvgMinMaxForField($select, $from, $field);
        }

        $select->addFields(array("EidM1"));
        $select->addFunction(array(
            "GasM1DateTime"    => "min(GasM1DateTime)",
            "GasM1"   => "min(GasM1)",
            "water"   => "min(water)"
        ));

        $this->addAvgMinMaxForField($select, $from, "WaterFlow");
        $select->addGroupBy($key);
        $where=new clause("datetime is not null");
        $where->addAnd(new clause("water is not null"));
        $select->where($where);

        $insert=new insert(static::TABLE);
        $insert->subquery=$select;

        db::query($insert);

        // Now delete the last line from the table, because that data may be based on an incomplete period
        $delete=new delete(static::TABLE);
        $delete->addOrder("datetime DESC");
        $delete->addLimit(1);
        $delete->allowDeleteAll()->execute();

    }

    private function addAvgMinMaxForField(select &$select, repositoryInterface $from, string $field) {
        if ($from instanceof \collector\dsmr\repository\repository) {
            $select->addFunction(array(
                "avg". $field => "avg(" . $field . ")",
                "min". $field => "min(" . $field . ")",
                "max". $field => "max(" . $field . ")",
            ));
        } else {
            $select->addFunction(array(
                "avg". $field => "avg(avg" . $field . ")",
                "min". $field => "min(min" . $field . ")",
                "max". $field => "max(max" . $field . ")",
            ));
        }
    }

    public function getLast() {
        $qry=new select(static::TABLE);
        $qry->addLimit(1);
        $qry->addOrder("datetime DESC");
        $result=db::query($qry);
        return $result->fetchObject("aggregate", array(new static));
    }

}

