var meter=function() {
    function update(data) {
        document.getElementById("meter-T1").innerHTML=parseFloat(data.T1).toFixed(3);
        document.getElementById("meter-T2").innerHTML=parseFloat(data.T2).toFixed(3);
        document.getElementById("meter-GasM1").innerHTML=parseFloat(data.GasM1).toFixed(3);
        if(data.tariff.charCodeAt(1)==1 || data.tariff=="1") {
            document.getElementById("meter-tariff-arrow").innerHTML="&#8598;"
        } else if(data.tariff.charCodeAt(1)==2 || data.tariff=="2") {
            document.getElementById("meter-tariff-arrow").innerHTML="&#8601;"
        } else {
            document.getElementById("meter-tariff-arrow").innerHTML="&#8604;"
        }
    }
    
    return {
        update:update
    };
}();

