var gasBarGraph=function() {
    function graph(container, title, period) {
        if (period == "month") {
            var url = "getJSON.php?data=monthGas";
        } else if (period == "year") {
            var url = "getJSON.php?data=yearGas";
        }
        var data=[]
        $.ajax({
            url: url,
            async: false,
            dataType: "json",
            success: function(json) {
                data=json;
            }
        });
        return new Highcharts.Chart({
            chart: {
                type: 'column',
                renderTo: container,
                useUTC: false 
            },
            title: {
                text: title
            },
            xAxis: {
                categories: data.days
            },
            yAxis: {
                min: 0
            },
            plotOptions: {
            },
            series: [{
                name: "Gebruik (m<sup>3</sup>)",
                data: data.data 
            }] 
        });
    }

    return {
        graph:graph,
    }
}();
