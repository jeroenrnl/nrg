var gaugeWatt=function() {

    function gauge(container, title, max) {
        var gaugeTitle = "Watt";
        return new Highcharts.Chart({
            chart: {
                renderTo: container,
                type: 'gauge',
                plotBackgroundColor: null,
                plotBackgroundImage: null,
                plotBorderWidth: 0,
                plotShadow: false 
            },

            title: {
                text: title
            },
            pane: {
                startAngle: -150,
                endAngle: 150,
                background: [{
                    backgroundColor: {
                        linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                        stops: [
                            [0, '#FFF'],
                            [1, '#222']
                        ]
                    },
                    borderWidth: 0,
                    outerRadius: '109%'
                }, {
                    backgroundColor: {
                        linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                        stops: [
                            [0, '#333'],
                            [1, '#FFF']
                        ]
                    },
                    borderWidth: 1,
                    outerRadius: '107%'
                }, {
                    // default background
                }, {
                    backgroundColor: '#DDD',
                    borderWidth: 0,
                    outerRadius: '105%',
                    innerRadius: '103%'
                }]
            },

            // the value axis
            yAxis: {
                min: -max,
                max: max,

                minorTickInterval: max/30,

                tickInterval: max/6,
                tickWidth: 3,
                tickPosition: 'inside',
                tickLength: 12,
                tickColor: '#444',
                labels: {
                    step: 1,
                    rotation: 'auto'
                },
                title: {
                    text: gaugeTitle
                },
                plotBands: [{
                    from: -max,
                    to: 0,
                    color: {
                        linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
                        stops: [
                            [0, '#00ff00'],
                            [1, '#55bf3b']
                        ]
                    }
                }, {
                    from: 0,
                    to: max,
                    color: {
                        linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
                        stops: [
                            [0, '#ffff00'],
                            [1, '#ff0000']
                        ]
                    }
                }]
            },

            series: [{
                name: 'Current usage',
                data: [0],
                tooltip: {
                    valueSuffix: 'Watt'
                }, 
                yAxis: 0
            }]
        });
    }

    function update(widget, field, fieldR, data) {
        var pointTotal = widget.series[0].points[0];
        pointTotal.update(parseFloat(data[field])*1000 - parseFloat(data[fieldR])*1000);
    }

    return {
        gauge:gauge,
        update:update
    };
}();
