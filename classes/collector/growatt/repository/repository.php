<?php
namespace collector\growatt\repository;

use db\clause;
use db\db;
use db\insert;
use db\param;
use db\select;

use collector\growatt\measurement;

use DateTime;
use DateTimeZone;
use repositoryInterface;

use PDO;
use stdClass;



class repository implements repositoryInterface {
    const TABLE="growatt";

    public function __construct() {
    }

    public function load($id) {

    }

    public function save($measurement) {
        $qry=new insert(static::TABLE);

        foreach ($measurement->getData() as $label => $value) {
            $param=new param(":" . $label, $value, PDO::PARAM_STR);
            $qry->addParam($param);
        }
        $qry->execute();
    }

    public function getAll() {

    }

    public function getLast() {
        $qry=new select(static::TABLE);
        $qry->addLimit(1);
        $qry->addOrder("datetime DESC");
        $result=db::query($qry);
        $measurement = new \collector\growatt\measurement(new static);
        $measurement->data = $result->fetch(PDO::FETCH_ASSOC);
        return $measurement;
    }

    public function getLastDay() {
        $qry=new select(static::TABLE);
        $qry->addFields(array("datetime", "Pac"));
        $qry->addOrder("datetime");
        $qry->where(new clause("datetime >= DATE_SUB(NOW(), INTERVAL 2 DAY)"));
        $result=db::query($qry);
        $measurements=$result->fetchAll(PDO::FETCH_ASSOC);

        $Pac=new StdClass();
        $Pac->name="Opbrengst";
        $Pac->data=array();

        foreach ($measurements as $measurement) {
            $date = DateTime::createFromFormat('Y-m-d H:i:s', $measurement["datetime"], new DateTimeZone("UTC"))->getTimestamp()*1000;
            $Pac->data[]=array($date, (float) $measurement["Pac"]);
         }
         return array(
            $Pac
         );
    }

    public function getMonthEacToday() {

        $dates = new select(static::TABLE);
        $dates->addFunction(array("date" => "max(datetime)"));
        $dates->where(new clause("EacToday > 0"));
        $dates->addGroupBy("date(datetime)");

        $qry = new select(static::TABLE);
        $qry->addFunction(array("date" => "date(datetime)"));
        $qry->addFields(array("EacToday"));
        $qry->where(clause::inSubQry("datetime", $dates));
        $qry->addLimit(30);
        $qry->addOrder("datetime DESC");

        $reverse = new select(array("month" => $qry));
        $reverse->addOrder("date");
        $result=db::query($reverse);
        $measurements=$result->fetchAll(PDO::FETCH_CLASS,"StdClass");

        $month = new StdClass();
        $month->name="Opbrengst per dag";
        $month->data=array();
        $month->days = array();

        foreach ($measurements as $measurement) {
            $month->days[] = $measurement->date;
            $month->data[]=(float) $measurement->EacToday;
        }
        return $month;
    }

    public function getHealth(int $minutes) {
        $qry=new select(self::TABLE);
        $qry->addFunction(array("count" => "count(*)"));
        $qry->where(new clause("datetime > (date_sub(now(), interval " . $minutes . " minute))"));
        return $qry->getCount() !== 0;
    }

}

