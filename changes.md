alter table measurement add column water float(11,3);
alter table measurement add column water_unit char(10);
alter table measurement add column WaterFlow float(12,6);

alter table aggregate_minute add column water float(12,3);
alter table aggregate_hour add column water float(12,3);

alter table aggregate_minute add column avgWaterFlow float(12,6);
alter table aggregate_minute add column minWaterFlow float(12,6);
alter table aggregate_minute add column maxWaterFlow float(12,6);
alter table aggregate_hour add column avgWaterFlow float(12,6);
alter table aggregate_hour add column minWaterFlow float(12,6);
alter table aggregate_hour add column maxWaterFlow float(12,6);
